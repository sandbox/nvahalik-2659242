<?php

/**
 * @file
 * Installation hooks.
 */

/**
 * Convert wish lists to use orders.
 */
function commerce_wishlist_update_7201(&$sandbox) {
  if (!isset($sandbox['wishlist']['progress'])) {
    $sandbox['wishlist']['progress'] = 0;
    $sandbox['wishlist']['current_id'] = 0;
    $sandbox['wishlist']['max'] = db_query('SELECT COUNT(DISTINCT wishlist_id) FROM {commerce_wishlist}')->fetchField();
  }

  // Get the next four coupon ids.
  $wishlist_ids = db_select('commerce_wishlist', 'cw')
    ->fields('cw', array('wishlist_id', 'uid'))
    ->condition('wishlist_id', $sandbox['wishlist']['current_id'], '>')
    ->range(0, 10)
    ->orderBy('wishlist_id', 'ASC')
    ->execute()
    ->fetchAll();

  // Update the coupons.
  if ($wishlist_ids) {
    foreach ($wishlist_ids as $wishlist) {
      $product_ids = db_select('commerce_wishlist_item', 'cwi')
        ->fields('cwi')
        ->condition('wishlist_id', $wishlist->wishlist_id)
        ->execute()
        ->fetchAll();

      if (!$product_ids) {
        $sandbox['wishlist']['progress']++;
        $sandbox['wishlist']['current_id'] = $wishlist->coupon_id;
        continue;
      }

      $wishlist_order = commerce_wishlist_order_new($wishlist->uid);

      foreach ($product_ids as $wishlist_product) {
        $product = commerce_product_load($wishlist_product->product_id);

        $line_item = commerce_product_line_item_new($product, 1);
        $line_item->created = $wishlist_product->added;
        $line_item->quantity = $wishlist_product->quantity > 1 ? $wishlist_product->quantity : 1;
        $line_item->commerce_display_path[LANGUAGE_NONE][0] = array('value' => 'node/' . $wishlist_product->nid);

        // Set the incoming line item's order_id.
        $line_item->order_id = $wishlist_order->order_id;

        // Save the incoming line item now so we get its ID.
        commerce_line_item_save($line_item);

        // Add it to the order's line item reference value.
        $wishlist_order->commerce_line_items[LANGUAGE_NONE][] = array('line_item_id' => $line_item->line_item_id);
      }

      // Save the updated order.
      commerce_order_save($wishlist_order);

      $sandbox['wishlist']['progress']++;
      $sandbox['wishlist']['current_id'] = $wishlist->coupon_id;
    }
  }
  else {
    $sandbox['wishlist']['#finished'] = 1;
  }

  $sandbox['wishlist']['#finished'] = empty($sandbox['wishlist']['max']) ? 1 : ($sandbox['wishlist']['progress'] / $sandbox['wishlist']['max']);
}

/**
 * Delete the old wishlist tables from the database.
 */
function commerce_wishlist_update_7202() {
  db_delete('commerce_wishlist');
  db_delete('commerce_wishlist_item');
}

/**
 * Implements hook_uninstall().
 */
function commerce_wishlist_uninstall() {
  // Remove module variables.
  variable_del('commerce_wishlist_element');
  variable_del('commerce_wishlist_product_types');
  variable_del('commerce_wishlist_weight');
}
